<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    protected $table = 'perfil';
    protected $fillable = ['nome'];

    public $timestamps = false;

    public function usuarios() {
        return $this->hasMany('App\Usuario', 'id_perfil', 'id');
    }
}
