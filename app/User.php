<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'usuario';
    protected $fillable = ['nome', 'email', 'senha', 'data_nascimento'];

    public function perfil()
    {
        return $this->hasOne('App\Perfil', 'id', 'id_perfil');
    }

    public function pontos() 
    {
        return $this->hasMany('App\Ponto', 'id_usuario', 'id');
    }

    public function medalhas() 
    {
        return $this->belongsToMany('App\Medalha', 'medalha_x_usuario', 'id_medalha', 'id_usuario');
    }
}
