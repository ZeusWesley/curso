<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ponto extends Model
{
    protected $table = 'ponto';
    protected $fillable = ['pontos', 'descricao'];

    public $timestamps = false;

    public function usuario() {
        return $this->hasOne(\App\Usuario::class, 'id', 'id_usuario');
    }
}
