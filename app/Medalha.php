<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medalha extends Model
{
    protected $table = 'medalha';
    protected $fillable = ['nome', 'descricao', 'pontos'];

    public function usuarios() {
        return $this->belongsToMany(\App\Medalha::class, 'medalha_x_usuario', 'id_medalha', 'id_usuario');
    }
}
